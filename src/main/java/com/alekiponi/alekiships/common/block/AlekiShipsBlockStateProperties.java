package com.alekiponi.alekiships.common.block;

import net.minecraft.world.level.block.state.properties.IntegerProperty;

public class AlekiShipsBlockStateProperties {
    public static final IntegerProperty FRAME_PROCESSED = IntegerProperty.create("frame_processed", 0, 3);
}