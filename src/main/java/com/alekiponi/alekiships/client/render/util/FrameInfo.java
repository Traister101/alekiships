package com.alekiponi.alekiships.client.render.util;

public record FrameInfo(int index, int x, int y) {
}